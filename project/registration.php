<?php
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<title> Registration page  </title>
	<link rel="stylesheet" type="text/css" href="styling.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
<?php include 'connection.php'; ?>
<script type="text/javascript">
var count = 6;
var redirect = "login.html";
 
function countDown(){
    var timer = document.getElementById("timer");
    if(count > 0){
        count--;
        timer.innerHTML = "This page will redirect to login page in "+count+" seconds.";
        setTimeout("countDown()", 1000);
    }else{
        window.location.href = redirect;
    }
}

</script>
 


<script type="text/javascript">
	
	function cancel() 
	{
		location.href = "login.html";
	}
</script>

</head>
<body style="background-image: url('login-background.jpg');">
	<nav class="navbar navbar-expand-sm bg-dark">
		
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="homepage.html">home</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="page1.html">page1 </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="page2.html">page 2</a>
    </li>
   
  </ul>
  <form class="form-inline" action="https://www.google.com/" style="text-align: right;">
    <input class="form-control mr-sm-2" type="text" placeholder="Search">
    <button class="btn1 btn-success" type="submit">Search</button>
  </form>
</nav>

	<?php
	$name = $email = $id = $Password = " ";

	

	if($_SERVER["REQUEST_METHOD"]=="POST")
		{
			if(empty($_POST['id']))
				{
					$idErr = " Id is required";
				}
			else
				{
				 	 $id = test_input($_POST['id']);
				 }
		
			if(empty($_POST['name']))
				{
					$nameErr  = " Name is required";
				}
			else
				{
				 	 $name  = test_input($_POST['name']);
				 }
				 if(empty($_POST['passwd']))
				{
					$passErr  = " Password is required";
				}
			else
				{
				 	 $Password  = test_input($_POST['passwd']);
				 }


			if(empty($_POST['email']))
				{
					$emailErr  = " Email is required";
				}
			else
				{
				 	 $email   = test_input($_POST['email']);
				 }
	
}

		function test_input($data)
		{
			$data = trim($data);


			return $data;

		}	
	
		$sql = "INSERT INTO NEWTABLE(ID,NAME,PASSWORD,EMAIL) VALUES($id,'$name','$Password','$email')";
		
		if(!mysqli_query($conn,$sql))
		{
			
		}
		else
			 {
			 	echo "<h1> Registered Successfully  </h1>";
			 	echo "<span id='timer'><script type='text/javascript'>countDown();</script>";
			 }

	?>
	<h1 style="text-align: center;">Registration page </h1>
<div class="formclass" style="text-align:center; " >

	<form method="post" action = "registration.php"><br><br>

			<label style="margin-right: 195px; "> Id   :</label>
	 <br><input type="text" name="id" placeholder="ID"><br>
	 <label style="margin-right: 175px;"> Name :</label>

	 <br><input type="text " name="name" placeholder="name"><br>
	<label style="margin-right: 150px; "> Password: </label>
	<br> <input type="Password" name="passwd" placeholder="Password"><br>
	<label style="margin-right: 175px; ">	Email: </label>
<br><input type="email" name="email" placeholder="email"><br>

	<br><br> <input type="Submit" name="Submit" value="SUBMIT"><br><br>
	
	
	</form>
	<br><button onclick="cancel()">Go to login page</button>
</div>

	   <div class = "social1">
	  	
	  	
	  	<a class = "" href="#">
	  		<i class = "fab fa-facebook-f">
	  			
	  		</i>
	  	</a> 

	  	<a class = "" href="#">
	  		<i class = "fab fa-twitter">
	  			
	  		</i>
	  	</a>

	  		<a class = "" href="#">
	  		<i class = "fab fa-google">
	  			
	  		</i>
	  	</a>  
	  		<a class = "" href="#">
	  		<i class = "fab fa-instagram">
	  			
	  		</i>
	  	</a> 
	  		<a class = "" href="#">
	  		<i class = "fab fa-youtube">
	  			
	  		</i>
	  	
	  	</a> 
	  </div>

</body>
</html>