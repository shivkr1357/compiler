<?php
session_start();

?>

<!DOCTYPE html>
<html>
<head>
	<title> Registration page  </title>
	<link rel="stylesheet" type="text/css" href="styleregpage.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
<?php include 'connection.php'; ?>


<script type="text/javascript">

var idnumber = <?php echo(mt_rand(100000,999999)); ?> ;
//console.log(idnumber);
var count = 6;
var redirect = "index.html";
var concatanatedString = "";
 
function countDown(){
    var timer = document.getElementById("timer");
    if(count > 0){
        count--;
        timer.innerHTML = "This page will redirect to login page in "+count+" seconds.";
        setTimeout("countDown()", 1000);
    }else{
        window.location.href = redirect;
    }
}

</script>
 


<script type="text/javascript">
	var data;
	function cancel() 
	{
		location.href = "index.html";
	}
</script>
<script type="text/javascript">

	$(document).ready(function(){
 
 $("#mname1").focus(function(){
 	
 	data = $('input[name="fname"]').val();
 	//console.log(data)
 	concatanatedString = data.concat(idnumber);
 	//console.log(concatanatedString);
  $("#id1").val(concatanatedString);
   
  });
});

</script>
<script type="text/javascript">
</script>

</head>
<body style="background-image: url('./pictures/login-background.jpg');">
	<nav class="navbar navbar-expand-sm bg-dark">
		
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="index.html">home</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="page1.html">page1 </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="page2.html">page 2</a>
    </li>
   
  </ul>
  
</nav>

	<?php

	$fname = $mname = $lname = $email = $id = $Password = " ";
	$nameErr = $emailErr = $idErr = $passErr  = " ";
	


	if($_SERVER["REQUEST_METHOD"]=="POST")
		{
			
			if(empty($_POST['id']))
			{

			}
			else
			{
				$id = test_input($_POST['id']);	
			}
			
				
			
		
			if(empty($_POST['fname']))
				{
					$nameErr  = " Name is required";
				}
			else
				{
				 	 $fname  = test_input($_POST['fname']);
				 }

			if(empty($_POST['mname']))
				{
					$nameErr  = " Name is required";
				}
			else
				{
				 	 $mname  = test_input($_POST['mname']);
				 }
				 

			if(empty($_POST['lname']))
				{
					$nameErr  = " Name is required";
				}
			else
				{
				 	 $lname  = test_input($_POST['lname']);
				 }


			if(empty($_POST['email']))
				{
					$emailErr  = " Email is required";
				}
			

			else
				{
				 	 $email   = test_input($_POST['email']);
				 }

			if(empty($_POST['passwd']))
				{
					$passErr  = " Password is required";
				}
			else
				{
				 	  $tempPassword= test_input($_POST['passwd']);
				 	  $Password =md5($tempPassword);
				 }


			
	$sql = "INSERT INTO REGISTRATION(ID,FIRST_NAME,MIDDLE_NAME,LAST_NAME,EMAIL,PASSWORD) VALUES('$id','$fname','$mname','$lname','$email','$Password')";
	
$sql1 = "INSERT INTO Student_info_table(ID,PROBLEM_SOLVED,SOLUTION_SUBMITTED,SOLUTION_ACCEPTED,WRONG_ANSWER,COMPILE_ERROR,RUNTIME_ERROR) VALUES('$id',0,0,0,0,0,0)";
$sql2="INSERT INTO AUTOSAVE_DATA(ID,C_DATA,CPP_DATA,JAVA_DATA,PYTHON_DATA) VALUES('$id','NULL','NULL','NULL','NULL')";
$sql3 = "INSERT INTO QUESTIONS_DETAILS(ID,QUESTION_ID,QUESTION_DATA) VALUES('$id','NULL','NULL')";

$sql4="INSERT INTO PRACTICE_SAVE_DATA(ID,C_DATA,CPP_DATA,JAVA_DATA,PYTHON_DATA) VALUES('$id','NULL','NULL','NULL','NULL')";

		if(!mysqli_query($conn,$sql))
		{
			echo "not inserted".mysqli_error($conn);	 	
		}
		else
			 {

				mysqli_query($conn,$sql1);
				mysqli_query($conn,$sql2);
				mysqli_query($conn,$sql3);
				mysqli_query($conn,$sql4);

				echo "<h1> Registered Successfully  </h1>";
				//echo $value;
			 	echo "<span id='timer'><script type='text/javascript'> idnumber+=1;countDown();</script>";

			 	
			 }
}

		function test_input($data)
		{
			$data = trim($data);


			return $data;

		}	
	
		
		

	?>
	<div class="header">
	<h2>REGISTER</h2>
</div><br>

	<form method="post" action="registration.php">
	
		<div class="input-group-mb-3">
		<br><label>ID</label>
		<br><input type="text" name="id" id="id1" placeholder="Auto filled (Don't fill it)">
	</div>
	
	
	<div class="input-group-mb-3" style="display: inline-block;">
		<br><label>FIRST NAME</label>
		<br><input type="text" name="fname" id="fname1" placeholder="first name " required />
		 <span class="error" id="fname2"> <?php echo $nameErr;?> 
		 </span>
		</div>
	

	<div class="input-group-mb-3" style="display: inline-block;">
		<br><label>MIDDLE NAME </label>
		<br><input type="text" name="mname" id="mname1" placeholder="middle name " >
		 <span class="error"> <?php echo $nameErr;?></span>
		</div>
	

	<div class="input-group-mb-3" style="display: inline-block;">

		<br><label>LAST NAME</label>
		<br><input type="text" name="lname" placeholder="last name" required />
		 <span class="error"> <?php echo $nameErr;?></span>
	</div>


<div class="input-group-mb-3">
		<br><label>Email</label>
		<br><input type="email" name="email" placeholder="Email id" required />
		<span class="error"> <?php echo $emailErr;?></span>
	</div>


	<div class="input-group-mb-3">
		<br><label>Password</label>
		<br><input type="password" name="passwd" value="" placeholder="Password" required />
		<span class="error"> <?php echo $passErr;?></span>
	</div>
	

	<br><div class="input-group-mb-3">
		<button type="submit" class="btn" name="submit">Register</button>
	</div>
	<p>
		Already a member? <a href="index.html">Sign in</a><br>
	</p>
</form>
</div>

	   <div class = "social1">
	  	
	  	
	  	<a class = "" href="#">
	  		<i class = "fab fa-facebook-f">
	  			
	  		</i>
	  	</a> 

	  	<a class = "" href="#">
	  		<i class = "fab fa-twitter">
	  			
	  		</i>
	  	</a>

	  		<a class = "" href="#">
	  		<i class = "fab fa-google">
	  			
	  		</i>
	  	</a>  
	  		<a class = "" href="#">
	  		<i class = "fab fa-instagram">
	  			
	  		</i>
	  	</a> 
	  		<a class = "" href="#">
	  		<i class = "fab fa-youtube">
	  			
	  		</i>
	  	
	  	</a> 
	  </div>


</body>
</html>