var text;
var editor;
var y;


     var CMode = "ace/mode/c_cpp";
     var CppMode = "ace/mode/c_cpp";
     var JavaMode = "ace/mode/java";
     var PythonMode = "ace/mode/python";

 var x;
 var editortheme;
 function selectTheme()
  {
editortheme = document.getElementById("themeSelect").value;
switch(editortheme)
  {
    case 'Monokai':
         editor.setTheme("ace/theme/monokai");
         break;
         
    case 'tomorrow_night_bright':
         editor.setTheme("ace/theme/tomorrow_night_bright");
         break;
   case 'tomorrow_night':
         editor.setTheme("ace/theme/tomorrow_night");
         break;
  
    case 'twilight':
         editor.setTheme("ace/theme/twilight");
         break;
    case 'tomorrow_night_blue':
         editor.setTheme("ace/theme/tomorrow_night_blue");
         break;
                    
   case 'Eclipse':
         editor.setTheme("ace/theme/eclipse");
         break;

   case 'dreamweaver':
         editor.setTheme("ace/theme/dreamweaver");
         break;

   case 'gob':
         editor.setTheme("ace/theme/gob");
          break;
   case 'gruvbox':
         editor.setTheme("ace/theme/gruvbox");
          break;
  case 'Idlefingers':
         editor.setTheme("ace/theme/idle_fingers");
          break;
    
  case 'solarized_dark':
         editor.setTheme("ace/theme/solarized_dark");
          break;
  
  case 'solarized_light':
         editor.setTheme("ace/theme/solarized_light");
          break;
    
  case 'kuroir':
         editor.setTheme("ace/theme/kuroir");
          break;

   case 'merbivore':
         editor.setTheme("ace/theme/merbivore");
         break;
  case 'iplastic':
         editor.setTheme("ace/theme/iplastic");
         break;
                          
  }


  }
  function selectOption()
   {
     
     

    var c= `
//You can edit your code here
#include<stdio.h>
int main()
 {
  printf("Hello C");
  return 0;
 }
`;
var cp = `
//You can edit your code here
#include<iostream>
using namespace std;
int main()
 {
  cout<<"hello ";
  return 0;
 }
`;
var ja =`
//You can edit your code here
class Main
 {
  public static void main(String args[])
  {
    System.out.println("Hello JAVA");
  }
}
`; 

var py = `
#You can edit your code here
print("hello")`;

     x = document.getElementById("unique").value;
    console.log(x);
    
    switch(x)
        {
          
          case 'C':
          //document.getElementById("code").innerHTML = c;
          editor.setValue(c);
          
          Editors(CMode);
          break;
          case 'CPP': 
          //document.getElementById("code").innerHTML = cp;
          //var temp=document.getElementById("code");
          editor.setValue(cp);
          //console.log(temp);
          Editors(CppMode);
          break;
          case 'JAVA': 
          //document.getElementById("code").innerHTML = ja;
          editor.setValue(ja);
          Editors(JavaMode);
     

          break;
          case 'PYTHON':
          //document.getElementById("code").innerHTML =  py;
          Editors(PythonMode);
          editor.setValue(py);
          break;
          case 'JAVASCRIPT': 
          window.location.href = "editor.html";
          break;
           
        }
}
  

             
             
            function Editors(modex)
          {   
              //editor.session.setValue(text);
              //console.log(text);
              // text = $("textarea[id='code']");
               //text.val(editor.getSession().getValue());
               //console.log(editor.getSession().getValue());
             
              editor.getSession().setMode(modex);
             
            text = editor.getValue();
                   
              editor.focus();
              
             
              
             // editor.setValue(z);
              editor.renderer.setOptions(
                {
                  fontFamily:"Courier,monospace",
                  fontSize:"10pt",
                  maxLines: Infinity,
                  showLineNumber:false,
                  showGutter:true,
                  vScrollBarAlwaysVisible:true,
                  enableBasicAutocompletion:false,
                  enableLiveAutocompletion:false,
                  setBehaviourEnabled:false,

                });
             // editor.setShowPrintMargin(false);

             } 


$(document).ready(function(){

   editor=ace.edit("code");
     
    console.log("ready0");
    
     $("#run").click(function(){
           $("#output").html("Loading......");
         
     });
     
     $("#submit").click(function(){
           $("#output").html("Loading......");
         
     });


  });



function runFunction()
  {

    tagFile="";
  //  console.log(x);
    if(x == "C")
       {
         tagFile = "c.php";

       }
     else if(x == "CPP")
       {
         tagFile = "cpp.php";
       }
    else if(x == "JAVA")
       {
         tagFile = "java.php";
       }
    else if(x == "PYTHON")
       {
         tagFile = "python.php";
       }


  text=editor.getValue();
  y = document.getElementById("input").value;
 
  
  
  

$("form").submit(function(e) {
    e.preventDefault();
 
});
      // AJAX
      $.ajax({

            type: "POST", 
            cache: false, 
             data: {code:text,input:y},
            url: tagFile,
            datatype: "html", 
            success: function(result) { 
                $('#output').html(result);
               
            }
        });

}

function saveFunction()
 
  {
    text=editor.getValue();
    $("form").submit( function(e) {
       e.preventDefault();
       
        });
     $.ajax({
        url: "autosave.php",
        type: "POST",
        data: {code:text,unique:x},
        success: function(data) {
           if (data) {
                alert("saved");
            }
        }
    });
}
function practiceSaveFunction()
 
  {
    text=editor.getValue();
    $("form").submit( function(e) {
       e.preventDefault();
       
        });
     $.ajax({
        url: "practiceSaveData.php",
        type: "POST",
        data: {code:text,unique:x},
        success: function(data) {
           if (data) {
                alert("saved");
            }
        }
    });
}


function openFunction()
 {
  
  $("form").submit( function(e) {
       e.preventDefault();
        });

   $.ajax({
        url: "open.php",
        type: "POST",
        data: {unique:x},
        success: function(data) {
        
          editor.setValue(data);
        }
    });
 }


 function Submit()
 {

  text = editor.getValue();


  $("form").submit( function(e) {
       e.preventDefault();
        });

   $.ajax({
        url: "submit.php",
        type: "POST",
        data: {code:text,unique:x},
        success: function(data) {
        $("#output").html(data);
        }
    });
 }
 

  function clearFunction()
   {
    editor.setValue("");
    document.getElementById("input").innerHTML = " ";
    document.getElementById("output").innerHTML = " ";      

   }
 
